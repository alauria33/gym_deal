//
//  PayoutCalculatorTest.swift
//  GymDealTests
//
//  Created by Andrew Lauria on 10/15/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import XCTest
@testable import GymDeal

class PayoutCalculatorTests: XCTestCase {
    let calculator = PayoutCalculator()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test() {
        var userResults = [UserResult]()
        let userResult1 = UserResult(id: 1, completedSessions: 0, pledgedSessions: 1, betAmount: 4)
        let userResult2 = UserResult(id: 2, completedSessions: 2, pledgedSessions: 4, betAmount: 2)
        let userResult3 = UserResult(id: 3, completedSessions: 0, pledgedSessions: 5, betAmount: 44)
        let userResult4 = UserResult(id: 4, completedSessions: 4, pledgedSessions: 5, betAmount: 3)
        let userResult5 = UserResult(id: 5, completedSessions: 0, pledgedSessions: 5, betAmount: 23)
        let userResult6 = UserResult(id: 6, completedSessions: 5, pledgedSessions: 5, betAmount: 76)
        let userResult7 = UserResult(id: 7, completedSessions: 7, pledgedSessions: 7, betAmount: 100)
        let userResult8 = UserResult(id: 8, completedSessions: 5, pledgedSessions: 5, betAmount: 100)

        userResults.append(userResult1)
        userResults.append(userResult2)
        userResults.append(userResult3)
        userResults.append(userResult4)
        userResults.append(userResult5)
        userResults.append(userResult6)
        userResults.append(userResult7)
        userResults.append(userResult8)

        let userPayouts = calculator.calculate(userResults: userResults)
        print()
        print()
        for (id, payout) in userPayouts {
            print("User \(id) receives $\(payout)")
        }
        print()
        print()
        
        var totalContributed = 0.0
        for userResult in userResults {
            totalContributed += userResult.betAmount
        }
        var totalPaid = 0.0
        for (id, payout) in userPayouts {
            totalPaid += payout
        }
        XCTAssertEqual(totalContributed, totalPaid + 5)
    }
}
