//
//  SignUpView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class LoginView: UIView {
    var submitAction: (() -> Void)?
    
    let loginLabel: UILabel = {
        let label = UILabel()
        label.text = "Login to your Account"
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 32)
        return label
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField(placeHolder: "Email")
        tf.backgroundColor = .white
        tf.tintColor = .black
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField(placeHolder: "Password")
        tf.backgroundColor = .white
        tf.tintColor = .black
        tf.autocapitalizationType = .none
        tf.isSecureTextEntry = true
        return tf
    }()
    
    let submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("  Submit  ", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.backgroundColor = UIColor.buttonGreen
        button.tintColor = .white
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
        
        self.addSubview(loginLabel)
        loginLabel.setAnchor(width: self.frame.width, height: self.frame.height * 0.08)
        loginLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loginLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * -0.3).isActive = true
        
        self.addSubview(emailTextField)
        emailTextField.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        emailTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: self.loginLabel.centerYAnchor, constant: self.frame.height * 0.14).isActive = true
        
        self.addSubview(passwordTextField)
        passwordTextField.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        passwordTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        passwordTextField.centerYAnchor.constraint(equalTo: self.emailTextField.centerYAnchor, constant: self.frame.height * 0.06).isActive = true
        
        addSubview(submitButton)
        submitButton.setAnchor(width: self.frame.width * 0.4, height: self.frame.height * 0.06)
        submitButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        submitButton.centerYAnchor.constraint(equalTo: self.passwordTextField.centerYAnchor, constant: self.frame.height * 0.13).isActive = true
    }
    
    @objc func handleSubmit() {
        submitAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
