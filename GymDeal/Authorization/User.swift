//
//  User.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import GooglePlaces
import Firebase
import Foundation

struct User {
    var id: String?
    var name: String?
    var gymLocation: CLLocationCoordinate2D?
//    
//    init?(snapshot: DataSnapshot) {
//        guard let value = snapshot.value as? [String: Any] else { return }
//        self.name = value["name"] as? String
//        if let gymLocation = value["gymLocation"] as? [String : Any] {
//            let lat = gymLocation["lat"] as? Double
//            let lon = gymLocation["lon"] as? Double
//            if lat != nil && lon != nil {
//                self.gymLocation = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
//            }
//        }
//    }
}
