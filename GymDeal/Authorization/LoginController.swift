//
//  ViewController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class LoginController: UIViewController {
    let authorizationApi = AuthorizationApi()
    var loginView: LoginView!
    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .backgroundColor
        
        setupView()
        
        NavigationManager.showDefault(controller: self, animated: true, action: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.loginView.emailTextField.becomeFirstResponder()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    func setupView() {
        self.loginView = LoginView(frame: self.view.frame)
        self.loginView.submitAction = submitPressed
        self.view.addSubview(loginView)
        loginView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    // because self has strong reference to closure, it can create memory leaks and cycles. prevented with weak
    func submitPressed() {
        guard let email = loginView.emailTextField.text else { return }
        guard let password = loginView.passwordTextField.text else { return }
        
        authorizationApi.login(email: email, password: password) { [weak self] (error) in
            if error != nil {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                alert.view.tintColor = UIColor.darkDullGreen
                self?.present(alert, animated: true)
            } else {
                guard let controller = self else { return }
                self?.authorizationApi.loadUserAndNavigate(controller: controller)
            }
        }
    }
}
