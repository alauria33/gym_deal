//
//  FirebaseApi.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import FBSDKLoginKit
import Firebase
import Foundation
import GooglePlaces
import UIKit

class AuthorizationApi {
    
    func updateGymLocation(location: CLLocationCoordinate2D) {
        guard let userId = userDefaults.userId else { return }

        print("Ay")
        userDefaults.gymLocation = location
        let userData: [String: Any] = [
            "gymLocation": [
                "lat" : location.latitude,
                "lon" : location.longitude
            ]
        ]
        db.child("users/\(userId)").updateChildValues(userData)
    }
    
    func signUp(name: String, email: String, password: String, completionHandler: @escaping (Error?) -> Void) {
        let userData: [String: Any] = [
            "name": name
        ]
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let err = error {
                print("/////////////////////////////")
                print("Error signing up user")
                print("/////////////////////////////")
                print(err.localizedDescription)
                print("/////////////////////////////")
                completionHandler(err)
            } else {
                guard let userId = result?.user.uid else { return }
                userDefaults.userId = userId
                db.child("users/\(userId)").updateChildValues(userData)
                completionHandler(nil)
            }
        }
    }
    
    func signUpWithFacebook(completionHandler: @escaping (Error?) -> Void) {
        let userData: [String: Any] = [
            "name": ""
        ]
        
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in
            if let err = error {
                completionHandler(err)
            } else {
                guard let userId = result?.user.uid else { return }
                db.child("users/\(userId)").updateChildValues(userData)
                userDefaults.userId = userId
                completionHandler(nil)
            }
        }
    }
    
    func delete(completionHandler: (Error?) -> Void) {
        do {
            let currentUser = Auth.auth().currentUser
            if let userId = currentUser?.uid {
                db.child("users/\(userId)").removeValue()
                db.child("pacts/\(userId)").removeValue()
            } else if let userId = userDefaults.userId {
                db.child("users/\(userId)").removeValue()
                db.child("pacts/\(userId)").removeValue()
            }
            try Auth.auth().signOut()
            if currentUser != nil {
                currentUser?.delete()
            }
            userDefaults.clearData()
            completionHandler(nil)
        } catch let error {
            print(error.localizedDescription)
            completionHandler(error)
        }
    }
    
    func logout(completionHandler: (Error?) -> Void) {
        do {
            try Auth.auth().signOut()
            userDefaults.clearData()
            completionHandler(nil)
        } catch let error {
            print(error.localizedDescription)
            completionHandler(error)
        }
    }
    
    func login(email: String, password: String, completionHandler: @escaping (Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let err = error {
                print(err.localizedDescription)
                completionHandler(err)
            } else {
                if let id = result?.user.uid {
                    userDefaults.userId = id
                }
                completionHandler(nil)
            }
        }
    }
    
    func loadUserAndNavigate(controller: UIViewController) {
        loadUser() { [weak self] (userResponse) in
            guard let user = userResponse else { return }
            guard let authUser = Auth.auth().currentUser else { return }
            var fromFacebook = false
            for userData in authUser.providerData {
                if userData.providerID == "facebook.com" {
                    fromFacebook = true
                }
            }
            // handles case where user terminates app before verifying
            if !fromFacebook && !authUser.isEmailVerified {
                let navController = UINavigationController(rootViewController: SignUpController())
                navController.pushViewController(EmailSignUpController(), animated: false)
                navController.pushViewController(EmailVerificationController(), animated: false)
                controller.present(navController, animated: true)
                return
            }
            if user.gymLocation == nil {
                let navController = UINavigationController(rootViewController: AddGymController())
                controller.present(navController, animated: true)
                return
            }
            userDefaults.gymLocation = user.gymLocation
            self?.loadPact() { (pact) in
                if pact == nil {
                    let navController = UINavigationController(rootViewController: PactController())
                    controller.present(navController, animated: true)
                    return
                }
                userDefaults.pact = pact
                let navController = UINavigationController(rootViewController: HomeController())
                controller.present(navController, animated: true)
            }
        }
    }
    
    func loadUser(completionHandler: @escaping (User?) -> Void) {
        print("LOAD")
        print(userDefaults.userId)
        guard let userId = userDefaults.userId else { return }
        db.child("users").child(userId).observeSingleEvent(of: .value) { (snapshot) in
            print("FOUND SNAPSHOT")
            print(snapshot.value as? [String: Any])
            print(".............")
            guard let data = snapshot.value as? [String: Any] else {
                completionHandler(nil)
                return
            }
            var user = User()
            user.id = userId
            user.name = data["name"] as? String
            if let gymLocation = data["gymLocation"] as? [String : Any] {
                let lat = gymLocation["lat"] as? Double
                let lon = gymLocation["lon"] as? Double
                if lat != nil && lon != nil {
                    user.gymLocation = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
                }
            }
            completionHandler(user)
        }
    }
    
    func loadPact(completionHandler: @escaping (Pact?) -> Void) {
        guard let userId = userDefaults.userId else { return }
        db.child("pacts").child(userId).observeSingleEvent(of: .value) { (snapshot) in
            guard let data = snapshot.value as? [String: Any] else {
                completionHandler(nil)
                return
            }
            var pact: Pact?
            let dayCount = data["dayCount"] as? Int
            let wagerAmount = data["wagerAmount"] as? Int
            if dayCount != nil && wagerAmount != nil {
                pact = Pact(dayCount: dayCount!, wagerAmount: wagerAmount!)
            }
            completionHandler(pact)
        }
    }
}
