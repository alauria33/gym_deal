//
//  SignUpOptionsView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import FBSDKLoginKit
import UIKit

class SignUpView: UIView {
    var createAction: (() -> Void)?
    var facebookAction: (() -> Void)?
    var googleAction: (() -> Void)?
    var loginAction: (() -> Void)?
    
    let createButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("  Sign up with Email  ", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.backgroundColor = UIColor.buttonGreen
        button.tintColor = .white
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleCreate), for: .touchUpInside)
        return button
    }()
    
    let facebookButton: UIButton = {
        let button = UIButton(type: .custom)
        button.imageView?.contentMode = .scaleAspectFit
        let fbImage = UIImage(named: "facebook")
        button.setImage(fbImage, for: .normal)
        button.tintColor = .red
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(handleFacebook), for: .touchUpInside)
        return button
    }()
    
    let googleButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign up with Google", for: UIControl.State.normal)
        button.backgroundColor = .gray
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleGoogle), for: .touchUpInside)
        return button
    }()
    
    let logInLabel: UILabel = {
        let label = UILabel()
        label.text = "Already have an account?"
        label.numberOfLines = 1
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.backgroundColor = UIColor.clear
        button.tintColor = .darkDullGreen
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.mediumGreen.cgColor, UIColor.white.cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.9)
        layer.addSublayer(gradient)
        
        let title = UIImageView()
        title.image = UIImage(named:"title")
        self.addSubview(title)
        title.setAnchor(width: self.frame.width * 0.85, height: self.frame.height * 0.25)
        title.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        title.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * -0.15).isActive = true
        
        self.addSubview(facebookButton)
        facebookButton.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.08)
        facebookButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        facebookButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * 0.14).isActive = true
        
        self.addSubview(createButton)
        createButton.setAnchor(width: self.frame.width * 0.7, height: self.frame.height * 0.06)
        createButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        createButton.centerYAnchor.constraint(equalTo: facebookButton.centerYAnchor, constant: self.frame.height * 0.075).isActive = true
        
        self.addSubview(logInLabel)
        logInLabel.setAnchor(width: self.frame.width * 0.7, height: self.frame.height * 0.08)
        logInLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: self.frame.width * -0.013).isActive = true
        logInLabel.centerYAnchor.constraint(equalTo: createButton.centerYAnchor, constant: self.frame.height * 0.1).isActive = true
        
        self.addSubview(loginButton)
        loginButton.setAnchor(width: self.frame.width * 0.2, height: self.frame.height * 0.1)
        loginButton.centerXAnchor.constraint(equalTo: logInLabel.centerXAnchor, constant: self.frame.width * 0.32).isActive = true
        loginButton.centerYAnchor.constraint(equalTo: logInLabel.centerYAnchor).isActive = true
    }
    
    @objc func handleCreate() {
        createAction?()
    }

    @objc func handleFacebook() {
        facebookAction?()
    }
    
    @objc func handleGoogle() {
        googleAction?()
    }
    
    @objc func handleLogin() {
        loginAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
