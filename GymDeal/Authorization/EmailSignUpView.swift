//
//  SignUpView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class EmailSignUpView: UIView {
    var nextAction: (() -> Void)?
    
    let createLabel: UILabel = {
        let label = UILabel()
        label.text = "Create your Account"
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 32)
        return label
    }()
    
    let userNameTextField: UITextField = {
        let tf = UITextField(placeHolder: "User Name")
        tf.backgroundColor = .white
        tf.tintColor = .black
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let emailTextField: UITextField = {
        let tf = UITextField(placeHolder: "Email")
        tf.backgroundColor = .white
        tf.tintColor = .black
        tf.autocapitalizationType = .none
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField(placeHolder: "Password")
        tf.backgroundColor = .white
        tf.tintColor = .black
        tf.autocapitalizationType = .none
        tf.isSecureTextEntry = true
        return tf
    }()

    let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("  Next  ", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.backgroundColor = UIColor.buttonGreen
        button.tintColor = .white
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
        
        self.addSubview(createLabel)
        createLabel.setAnchor(width: self.frame.width, height: self.frame.height * 0.08)
        createLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        createLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * -0.3).isActive = true
        
        self.addSubview(emailTextField)
        emailTextField.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        emailTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        emailTextField.centerYAnchor.constraint(equalTo: self.createLabel.centerYAnchor, constant: self.frame.height * 0.14).isActive = true
        
        self.addSubview(passwordTextField)
        passwordTextField.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        passwordTextField.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        passwordTextField.centerYAnchor.constraint(equalTo: self.emailTextField.centerYAnchor, constant: self.frame.height * 0.06).isActive = true
        
        addSubview(nextButton)
        nextButton.setAnchor(width: self.frame.width * 0.4, height: self.frame.height * 0.06)
        nextButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        nextButton.centerYAnchor.constraint(equalTo: self.passwordTextField.centerYAnchor, constant: self.frame.height * 0.13).isActive = true
    }
    
    @objc func handleNext() {
        nextAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
