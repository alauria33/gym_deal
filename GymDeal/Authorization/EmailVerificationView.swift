//
//  EmailVerificationView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class EmailVerificationView: UIView {
    var resendAction: (() -> Void)?
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Verify your Email"
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 34)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.textAlignment = .justified
        label.textColor = .black
        label.numberOfLines = 5
        var emailText = ""
        if let user = Auth.auth().currentUser {
            if let email = user.email {
                emailText = " to \(email)"
            }
        }
        label.text = "You must verify your email address to continue. Click below to re-send the link\(emailText), or go back to update your credentials."
        return label
    }()
    
    let resendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("  Resend Link  ", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.backgroundColor = UIColor.buttonGreen
        button.tintColor = .white
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleResend), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
        
        self.addSubview(titleLabel)
        titleLabel.setAnchor(width: self.frame.width, height: self.frame.height * 0.08)
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * -0.3).isActive = true
        
        self.addSubview(descriptionLabel)
        descriptionLabel.setAnchor(width: self.frame.width * 0.85, height: self.frame.height * 0.2)
        descriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        descriptionLabel.centerYAnchor.constraint(equalTo: self.titleLabel.centerYAnchor, constant: self.frame.height * 0.15).isActive = true
        
        self.addSubview(resendButton)
        resendButton.setAnchor(width: self.frame.width * 0.4, height: self.frame.height * 0.06)
        resendButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        resendButton.centerYAnchor.constraint(equalTo: self.descriptionLabel.centerYAnchor, constant: self.frame.height * 0.18).isActive = true
    }
    
    @objc func handleResend() {
        resendAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
