//
//  SignUpController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class EmailSignUpController: UIViewController {
    let authorizationApi = AuthorizationApi()
    var signUpView: EmailSignUpView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        setupView()
        
        NavigationManager.showDefault(controller: self, animated: true, action: nil)
    }
    
    func setupView() {
        self.signUpView = EmailSignUpView(frame: self.view.frame)
        self.signUpView.nextAction = nextPressed
        self.view.addSubview(signUpView)
        signUpView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.signUpView.emailTextField.becomeFirstResponder()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if self.isMovingFromParent {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    func nextPressed() {
        guard let name = signUpView.userNameTextField.text else { return }
        guard let email = signUpView.emailTextField.text else { return }
        guard let password = signUpView.passwordTextField.text else { return }
        
        authorizationApi.signUp(name: name, email: email, password: password) { [weak self] (error) in
            if error != nil {
                let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                alert.view.tintColor = UIColor.darkDullGreen
                self?.present(alert, animated: true)
            } else {
                let verificationController = EmailVerificationController()
                verificationController.sendVerification()
                guard let old = self else { return }
                NavigationManager.push(old: old, new: verificationController, animated: true)
            }
        }
    }
}
