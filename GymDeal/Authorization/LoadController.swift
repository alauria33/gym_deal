//
//  LoadController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class LoadController: UIViewController {
    let authorizationApi = AuthorizationApi()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = UIColor.buttonGreen
        
        loadApplication()
    }
    
    func loadApplication() {
        userDefaults.printData()
        if !userDefaults.hasRunBefore {
            authorizationApi.logout() { (error) in
                print(error)
            }
            userDefaults.hasRunBefore = true
        }
        
        if Auth.auth().currentUser != nil {
            Auth.auth().currentUser?.reload() { [weak self] (error) in
                self?.loadExistingUser()
            }
        } else {
            userDefaults.clearData()
            let navController = UINavigationController(rootViewController: SignUpController())
            self.present(navController, animated: false)
        }
    }
    
    func loadExistingUser() {
        var rootViewController: UIViewController!
        var verifyingEmail = false
        if let user = Auth.auth().currentUser {
            if userDefaults.userId == nil {
                userDefaults.userId = user.uid
            }
            var fromFacebook = false
            for userData in user.providerData {
                if userData.providerID == "facebook.com" {
                    fromFacebook = true
                }
            }
            // handles case where user terminates app before verifying
            if !fromFacebook && !user.isEmailVerified {
                rootViewController = SignUpController()
                verifyingEmail = true
            } else if userDefaults.gymLocation == nil {
                rootViewController = AddGymController()
            } else if userDefaults.pact == nil {
                rootViewController = PactController()
            } else {
                rootViewController = HomeController()
            }
        } else {
            userDefaults.clearData()
            rootViewController = SignUpController()
        }
        if verifyingEmail {
            let navController = UINavigationController(rootViewController: rootViewController)
            navController.pushViewController(EmailSignUpController(), animated: false)
            navController.pushViewController(EmailVerificationController(), animated: false)
            self.present(navController, animated: false)
        } else {
            let navController = UINavigationController(rootViewController: rootViewController)
            self.present(navController, animated: false)
        }
    }
}
