//
//  EmailVerificationController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/27/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class EmailVerificationController: UIViewController {
    let authorizationApi = AuthorizationApi()
    var showAlert = false
    var emailVerificationView: EmailVerificationView!
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        setupView()
        startTimer()
        
        NavigationManager.showDefault(controller: self, animated: true, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            authorizationApi.delete() { error in
                print(error)
            }
        }
    }
    
    func sendVerification() {
        if let user = Auth.auth().currentUser {
            timer?.invalidate()
            var actionCodeSettings = ActionCodeSettings.init()
            actionCodeSettings.handleCodeInApp = true
            actionCodeSettings.url = URL(string: "https://www.example.com")
            print(Bundle.main.bundleIdentifier!)
            actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
            user.sendEmailVerification() { [weak self] error in
                if error != nil {
                    print(error?.localizedDescription)
                    return
                }
                var emailText = ""
                if let email = user.email {
                    emailText = " to \(email)"
                }
                let alert = UIAlertController(title: "Link Sent", message: " A verification link has been sent\(emailText).", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                alert.view.tintColor = UIColor.darkDullGreen
                self?.present(alert, animated: true)
            }
            startTimer()
        }
    }
    
    func setupView() {
        self.emailVerificationView = EmailVerificationView(frame: self.view.frame)
        self.emailVerificationView.resendAction = resendPressed
        self.view.addSubview(emailVerificationView)
        emailVerificationView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: true)
    }

    func resendPressed() {
        sendVerification()
    }
    
    @objc func handleTimer() {
        guard let currentUser = Auth.auth().currentUser else { return }
        currentUser.reload()
        if currentUser.isEmailVerified {
            timer?.invalidate()
            let navController = UINavigationController(rootViewController: AddGymController())
            self.present(navController, animated: true)
        }
    }
}
