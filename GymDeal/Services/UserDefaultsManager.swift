//
//  UserDefaultsManager.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import Foundation
import GooglePlaces

class UserDefaultsManager {
    private let HAS_RUN_BEFORE = "HasRunBefore"
    private let USER_ID = "UserId"
    private let GYM_LOCATION = "GymLocation"
    private let PACT = "Pact"
    private let CHECK_IN = "CheckIn"
    
    private var deletableKeys: [String]!
    
    init() {
        deletableKeys = [USER_ID, GYM_LOCATION, PACT, CHECK_IN]
    }
    
    var hasRunBefore: Bool {
        get {
            return UserDefaults.standard.bool(forKey: HAS_RUN_BEFORE) ?? false
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: HAS_RUN_BEFORE)
        }
    }
    
    var userId: String? {
        get {
            return UserDefaults.standard.string(forKey: USER_ID)
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: USER_ID)
        }
    }
    
    var userIsLoggedIn: Bool {
        get {
            if let id = userId {
                return id != ""
            } else {
                return false
            }
        }
    }
    
    var gymLocation: CLLocationCoordinate2D? {
        get {
            guard let dict = UserDefaults.standard.object(forKey: GYM_LOCATION) as? NSDictionary else { return nil }
            guard let lat = dict["lat"] as? Double else { return nil }
            guard let lon = dict["lon"] as? Double else { return nil }
            return CLLocationCoordinate2D(latitude: lat, longitude:  lon)
        }
        set {
            guard let location = newValue else { return }
            let dict: NSDictionary = [
                "lat" : location.latitude,
                "lon" : location.longitude
            ]
            UserDefaults.standard.set(dict, forKey: GYM_LOCATION)
        }
    }
    
    var pact: Pact? {
        get {
            guard let dict = UserDefaults.standard.object(forKey: PACT) as? NSDictionary else { return nil }
            guard let dayCount = dict["dayCount"] as? Int else { return nil }
            guard let wagerAmount = dict["wagerAmount"] as? Int else { return nil }
            return Pact(dayCount: dayCount, wagerAmount: wagerAmount)
        }
        set {
            if newValue == nil {
                UserDefaults.standard.set(nil, forKey: PACT)
                return
            }
            guard let dayCount = newValue?.dayCount else { return }
            guard let wagerAmount = newValue?.wagerAmount else { return }
            let dict: NSDictionary = [
                "dayCount" : dayCount,
                "wagerAmount" : wagerAmount
            ]
            UserDefaults.standard.set(dict, forKey: PACT)
        }
    }
    
    var checkIn: (TimeInterval, CLLocationCoordinate2D)? {
        get {
            guard let dict = UserDefaults.standard.object(forKey: CHECK_IN) as? NSDictionary else { return nil }
            guard let time = dict["time"] as? TimeInterval else { return nil }
            guard let lat = dict["lat"] as? Double else { return nil }
            guard let lon = dict["lon"] as? Double else { return nil }
            return (time, CLLocationCoordinate2D(latitude: lat, longitude:  lon))
        }
        set {
            if newValue == nil {
                UserDefaults.standard.set(nil, forKey: CHECK_IN)
                return
            }
            guard let time = newValue?.0 else { return }
            guard let location = newValue?.1 else { return }
            let dict: NSDictionary = [
                "time" : time,
                "lat" : location.latitude,
                "lon" : location.longitude
            ]
            UserDefaults.standard.set(dict, forKey: CHECK_IN)
        }
    }
    
    func clearData() {
        let defaults = UserDefaults.standard
        for key in deletableKeys {
            defaults.removeObject(forKey: key)
        }
    }
    
    func printData() {
        print("/////////////////")
        print("userId = \(userId)")
        print("-----------------")
        print("gymLoc = \(gymLocation)")
        print("-----------------")
        print("pact = [\(pact?.dayCount), \(pact?.wagerAmount)]")
        print("-----------------")
        if checkIn != nil {
            print("checkIn = [\(checkIn?.0), \(checkIn?.1)]")
        } else {
            print("Not checked in")
        }
        print("/////////////////")
    }
}
