//
//  Util.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Foundation

func smartPrint(_ strings: String ...) {
    var strings = strings
    if strings.count == 0 {
        return
    }
    print()
    print()
    print("//////////////////////  \(strings.first!)   //////////////////////")
    print()
    strings.removeFirst()
    if strings.count > 0 {
        print("-----------------------------------------------")
        for string in strings {
            print(string)
            print("-----------------------------------------------")
        }
    }
    print()
    print("////////////////////////////////////////////")
    print()
    print()
}
