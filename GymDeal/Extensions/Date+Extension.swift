//
//  Date+Extension.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/30/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Foundation

extension TimeInterval {
    var localDate: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd, HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: TimeZone.current.abbreviation() ?? "UTC")
        
        return dateFormatter.string(from: date)
    }
}

extension Date {
    var startOfWeek: Int? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        guard let startOfWeek = gregorian.date(byAdding: .day, value: 1, to: sunday)?.timeIntervalSince1970 else { return nil }
        return Int(startOfWeek)
    }
}
