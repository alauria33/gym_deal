//
//  GMSAutocompleteViewController+Extension.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/27/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import GooglePlaces

extension GMSAutocompleteViewController {
    func boundAutocomplete(distance: Double) {
        guard let origin = locationManager.currentLocation else { return }
        print(origin)
        smartPrint("Origin: \(origin)")
        let northEastBound = locationWithBearing(bearing: 45, distance: distance, origin: origin)
        let southWestBound = locationWithBearing(bearing: 225, distance: distance, origin: origin)
        autocompleteBounds = GMSCoordinateBounds(coordinate: northEastBound, coordinate: southWestBound)
    }
    
    /*
     Taken from: https://stackoverflow.com/questions/7278094/moving-a-cllocation-by-x-meters/7278293#7278293
     north: bearing = 0, for east: bearing = 90, for southwest: bearing = 225, etc.
     */
    private func locationWithBearing(bearing :Double, distance: Double, origin: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let rDistance = distance / (6372797.6)
        let rbearing = bearing * Double.pi / 180.0
        
        let lat1 = origin.latitude * Double.pi / 180
        let lon1 = origin.longitude * Double.pi / 180
        
        let lat2 = asin(sin(lat1) * cos(rDistance) + cos(lat1) * sin(rDistance) * cos(rbearing))
        let lon2 = lon1 + atan2(sin(rbearing) * sin(rDistance) * cos(lat1), cos(rDistance) - sin(lat1) * sin(lat2))
        
        return CLLocationCoordinate2D(latitude: lat2 * 180 / Double.pi, longitude: lon2 * 180 / Double.pi)
    }
}
