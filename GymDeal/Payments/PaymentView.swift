//
//  PaymentView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class PaymentView: UIView {
    var buttonAction: (() -> Void)?
    let buttonButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Button", for: UIControl.State.normal)
        button.titleLabel?.font =  UIFont.boldSystemFont(ofSize: 24)
        button.backgroundColor = UIColor.lightGreen
        button.tintColor = .black
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
  
        addSubview(buttonButton)
        buttonButton.setAnchor(width: self.frame.width * 0.6, height: self.frame.height * 0.08)
        buttonButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        buttonButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * 0.2).isActive = true
    }
    
    @objc func handleButton() {
        buttonAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
