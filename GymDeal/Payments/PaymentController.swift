//
//  PaymentController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Braintree
import BraintreeDropIn
import Firebase
import FirebaseFunctions
import UIKit

class PaymentController: UIViewController {
    var paymentView: PaymentView!
    
    var functions: Functions!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .black
        
        paymentView = PaymentView(frame: self.view.frame)
        view.addSubview(paymentView)
        paymentView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        paymentView.buttonAction = buttonPressed
        
        functions = Functions.functions()
    }
    
    func buttonPressed() {
        print("PRESS")
        functions.httpsCallable("brainTreeTransaction").call(["text": "this is my input"]) { (result, error) in
            //TODO: Handle error
            if let error = error as NSError? {
                print(error)
            }
            print(result?.data)
            if let clientToken = (result?.data as? [String: Any])?["token"] as? String {
                print(clientToken)
                if clientToken != "" {
                    self.showDropIn(clientTokenOrTokenizationKey: clientToken)
                }
            }
        }
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentIcon
                // result.paymentDescription
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
    func postNonceToServer(paymentMethodNonce: String) {
        // Update URL with your server
        let paymentURL = URL(string: "https://your-server.example.com/payment-methods")!
        var request = URLRequest(url: paymentURL)
        request.httpBody = "payment_method_nonce=\(paymentMethodNonce)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) -> Void in
            // TODO: Handle success or failure
            }.resume()
    }
}
