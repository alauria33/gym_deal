//
//  PactView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class PactView: UIView {
    var dayPicker: UIPickerView!
    var wagerPicker: UIPickerView!
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 34)
        return label
    }()
    
    let dayLabel: UILabel = {
        let label = UILabel()
        label.text = "Select the amount of days you will work out each week."
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    let wagerLabel: UILabel = {
        let label = UILabel()
        label.text = "Select the amount of money you wager on each work out."
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    var nextAction: (() -> Void)?
    var nextButton: UIButton!
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
        
        addSubview(titleLabel)
        titleLabel.setAnchor(width: self.frame.width * 0.7, height: self.frame.height * 0.1)
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.topAnchor, constant: self.frame.height * 0.19).isActive = true
        
        addSubview(dayLabel)
        dayLabel.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        dayLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dayLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor, constant: self.frame.height * 0.13).isActive = true
        
        dayPicker = UIPickerView()
        dayPicker.tag = 0
        addSubview(dayPicker)
        dayPicker.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2.0)
        dayPicker.setAnchor(width: self.frame.height * 0.075, height: self.frame.width * 1.25)
        dayPicker.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        dayPicker.centerYAnchor.constraint(equalTo: dayLabel.centerYAnchor, constant: self.frame.height * 0.08).isActive = true
        dayPicker.layer.borderWidth = 0.0

        addSubview(wagerLabel)
        wagerLabel.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.1)
        wagerLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wagerLabel.centerYAnchor.constraint(equalTo: dayPicker.centerYAnchor, constant: self.frame.height * 0.12).isActive = true

        wagerPicker = UIPickerView()
        wagerPicker.tag = 1
        addSubview(wagerPicker)
        wagerPicker.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2.0)
        wagerPicker.setAnchor(width: self.frame.height * 0.075, height: self.frame.width * 1.25)
        wagerPicker.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wagerPicker.centerYAnchor.constraint(equalTo: wagerLabel.centerYAnchor, constant: self.frame.height * 0.08).isActive = true
        wagerPicker.layer.borderWidth = 0.0
        
        nextButton = UIButton(title: "", target: self, action: #selector(handleNext))
        nextButton.addToView(view: self, x: self.centerXAnchor, xMultiplier: 0, y: wagerPicker.centerYAnchor, yMultiplier: 0.18)
    }
    
    @objc func handleNext() {
        nextAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
