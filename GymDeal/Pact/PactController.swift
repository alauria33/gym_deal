//
//  PactController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class PactController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    let pactApi = PactApi()
    
    var create: Bool!
    var pactView: PactView!
    var dayCount: Int!
    var wagerAmount: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.backgroundColor
        
        setupView()
    }
    
    func setupView() {
        pactView = PactView(frame: self.view.frame)
        view.addSubview(pactView)
        pactView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        pactView.dayPicker.delegate = self
        pactView.dayPicker.dataSource = self
        pactView.wagerPicker.delegate = self
        pactView.wagerPicker.dataSource = self
        
        if let pact = userDefaults.pact {
            setupUpdate(pact: pact)
        } else {
            setupCreate()
        }
    }
    
    func setupCreate() {
        NavigationManager.showDefault(controller: self, animated: true, action: nil)
        dayCount = 1
        wagerAmount = 5
        pactView.titleLabel.text = "Create your pact"
        pactView.nextButton.setTitle("  Submit  ", for: UIControl.State.normal)
        pactView.nextAction = nextPressed
    }
    
    func setupUpdate(pact: Pact) {
        dayCount = pact.dayCount
        wagerAmount = pact.wagerAmount
        
        pactView.dayPicker.selectRow(dayCount - 1, inComponent: 0, animated: false)
        pactView.wagerPicker.selectRow(wagerAmount - 5, inComponent: 0, animated: false)
        pactView.titleLabel.text = "Update your pact"
        pactView.nextButton.setTitle("  Update  ", for: UIControl.State.normal)
        pactView.nextAction = updatePressed
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return 7
        } else {
            return 16
        }
    }
    
    // delegate method to return the value shown in the picker
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame:  CGRect(x: 0, y: 0, width: 100, height: 100))
        if pickerView.tag == 0 {
            label.text = "\(row + 1)"
        } else {
            label.text = "$\(row + 5)"
        }
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24)
        label.transform = CGAffineTransform(rotationAngle: (CGFloat.pi/2.0))
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        if pickerView.tag == 0 {
            return 50.0
        } else {
            return 65.0
        }
    }
    
    // delegate method called when the row was selected.
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            dayCount = row + 1
        } else {
            wagerAmount = row + 5
        }
    }

    func nextPressed() {
        pactApi.updatePact(dayCount: dayCount, wagerAmount: wagerAmount)
        self.present(UINavigationController(rootViewController: HomeController()), animated: true)
    }
    
    func updatePressed() {
        pactApi.updatePact(dayCount: dayCount, wagerAmount: wagerAmount)
        self.navigationController?.popViewController(animated: true)
    }
}
