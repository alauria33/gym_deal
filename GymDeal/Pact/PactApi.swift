//
//  PactApi.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/3/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class PactApi {
//    func getPact(completionHandler: @escaping (_ dayCount: Int, _ wagerAmount: Int) -> ()) {
//        guard let userId = userDefaults.userId else { return }
//
//        let ref = db.child("pacts/\(userId)")
//        ref.observeSingleEvent(of: .value, with: { (snapshot) in
//            guard let dict = snapshot.value as? NSDictionary else {
//                completionHandler(0, 0)
//                return
//            }
//            guard let dayCount = dict["dayCount"] as? Int, let wagerAmount = dict["wagerAmount"] as? Int else {
//                completionHandler(0, 0)
//                return
//            }
//            completionHandler(dayCount, wagerAmount)
//
//        }) { (error) in
//            print(error.localizedDescription)
//        }
//    }
    
    func updatePact(dayCount: Int, wagerAmount: Int) {
        guard let userId = userDefaults.userId else { return }

        userDefaults.pact = Pact(dayCount: dayCount, wagerAmount: wagerAmount)
        let ref = db.child("pacts/\(userId)")
        let sessionDict: [String: Any] = [
            "dayCount": dayCount,
            "wagerAmount": wagerAmount
        ]
        ref.updateChildValues(sessionDict)
    }
}
