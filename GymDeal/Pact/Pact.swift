//
//  Pact.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/13/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import Foundation

class Pact {
    var dayCount: Int
    var wagerAmount: Int
    
    init(dayCount: Int, wagerAmount: Int) {
        self.dayCount = dayCount
        self.wagerAmount = wagerAmount
    }
}
