//
//  SessionApi.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit

class SessionApi {
    
    func logSession() {
        guard let userId = userDefaults.userId else { return }
        guard let checkIn = userDefaults.checkIn else { return }
        guard let weekStart = Date().startOfWeek else { return }
        let checkOut = Date().timeIntervalSince1970
        
        let ref = db.child("sessions/\(userId)/\(weekStart)").childByAutoId()
        let sessionDict: [String: Any] = [
            "start": Int(checkIn.0),
            "end": checkOut,
            "lat" : checkIn.1.latitude,
            "lon" : checkIn.1.longitude
        ]
        ref.updateChildValues(sessionDict)
        
        userDefaults.checkIn = nil
        print(userDefaults.checkIn)
    }
    
    func getWeeklySessionCount(completionHandler: @escaping (_ count: Int) -> ()) {
        guard let userId = userDefaults.userId else { return }
        guard let weekStart = Date().startOfWeek else { return }
        
        let ref = db.child("sessions/\(userId)/\(weekStart)")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dict = snapshot.value as? NSDictionary else {
                completionHandler(0)
                return
            }
            completionHandler(dict.count)

        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
