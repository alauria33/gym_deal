//
//  HomeView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class HomeView: UIView {
    var sessionView: UIView!
    var shapeLayer: CAShapeLayer!
    var trackLayer: CAShapeLayer!
    var pulsatingLayer: CAShapeLayer!
    
    let progressLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 5
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let checkOutLabel: UILabel = {
        let label = UILabel()
        label.text = "Checkout in"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    let timeLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 28)
        return label
    }()
    
    var checkInAction: (() -> Void)?
    var checkInButton: UIButton!
    var checkOutAction: (() -> Void)?
    var checkOutButton: UIButton!
    
    func setup() {
        backgroundColor = UIColor.backgroundColor

        checkInButton = UIButton(title: "Hell", target: self, action: #selector(handleCheckIn))
        checkOutButton = UIButton(title: "Check Out", target: self, action: #selector(handleCheckOut))
        sessionView = UIView(frame: frame)
        addSubview(sessionView)
        
        let center = self.center

        progressLabel.frame = CGRect(x: 0, y: 0, width: frame.width * 0.9, height: frame.height * 0.2)
        progressLabel.center = CGPoint(x: center.x, y: center.y - frame.height * 0.25)
        addSubview(progressLabel)

        let circularPath = UIBezierPath(arcCenter: center, radius: 85, startAngle: -CGFloat.pi/2, endAngle: 2*CGFloat.pi, clockwise: true)
        pulsatingLayer = CAShapeLayer()
        pulsatingLayer.path = circularPath.cgPath
        pulsatingLayer.fillColor = UIColor.lightYellow.cgColor
        pulsatingLayer.position = center
        pulsatingLayer.bounds = bounds
        
        trackLayer = CAShapeLayer()
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGreen.cgColor
        trackLayer.lineWidth = 20
        trackLayer.fillColor = UIColor.backgroundColor.cgColor
        trackLayer.position = center
        trackLayer.bounds = bounds

        shapeLayer = CAShapeLayer()
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.mediumGreen.cgColor
        shapeLayer.lineWidth = 20
        shapeLayer.strokeEnd = 0.0005
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.position = center
        shapeLayer.bounds = bounds
        
        checkOutLabel.frame = CGRect(x: 0, y: 0, width: 140, height: 85)
        checkOutLabel.center = CGPoint(x: center.x, y: center.y - 20)
        timeLabel.frame = CGRect(x: 0, y: 0, width: 95, height: 85)
        timeLabel.center = CGPoint(x: center.x, y: center.y + 20)
        
        let tot = 7
        let totalW = Double(frame.width * 0.9)
        let w = totalW/Double(tot)
        let h = 50.0
        let xStart = (Double(frame.width) - totalW)/2.0
        let y = 300.0
        let radii = CGSize(width: 20, height: 20)
        let comp = 4
        for index in 0...tot-1 {
            let rectShape = CAShapeLayer()
            let x = xStart + Double(index) * w
            var corners: UIRectCorner = []
            if index == 0 {
                corners = [.bottomLeft , .topLeft]
            } else if index == tot-1 {
                corners = [.bottomRight , .topRight]
            }
            rectShape.path = UIBezierPath(roundedRect: CGRect(x: x, y: y, width: w, height: h), byRoundingCorners: corners, cornerRadii: radii).cgPath
            rectShape.position = center
            rectShape.bounds = bounds
            if index < comp {
                rectShape.fillColor = UIColor.mediumGreen.cgColor
            } else {
                rectShape.fillColor = UIColor.backgroundColor.cgColor
            }
            rectShape.strokeColor = UIColor.white.cgColor
            rectShape.lineWidth = 0.5
            self.layer.addSublayer(rectShape)
        }
        
        let outlineShape = CAShapeLayer()
        let corners: UIRectCorner = [.bottomLeft, .bottomRight, .topLeft, .topRight]
        outlineShape.path = UIBezierPath(roundedRect: CGRect(x: xStart, y: y, width: totalW, height: h), byRoundingCorners: corners, cornerRadii: radii).cgPath
        outlineShape.position = center
        outlineShape.bounds = bounds
        outlineShape.fillColor = UIColor.clear.cgColor
        outlineShape.strokeColor = UIColor.black.cgColor
        outlineShape.lineWidth = 1.3
        self.layer.addSublayer(outlineShape)
    }
    
    func removeSessionViews() {
        for subView in sessionView.subviews {
            subView.removeFromSuperview()
        }
        
        for subLayer in sessionView.layer.sublayers ?? [] {
            subLayer.removeFromSuperlayer()
        }
    }
    
    func showCheckInButton() {
        removeSessionViews()
        
        checkInButton.addToView(view: self, x: self.centerXAnchor, xMultiplier: 0, y: self.centerYAnchor, yMultiplier: 0.0)
    }
    
    func showCheckOutButton() {
        removeSessionViews()

        sessionView.addSubview(checkOutButton)
        checkOutButton.setAnchor(width: self.frame.width * 0.6, height: self.frame.height * 0.08)
        checkOutButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        checkOutButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * 0.0).isActive = true
    }
    
    func showProgressLayers() {
        removeSessionViews()

        sessionView.layer.addSublayer(pulsatingLayer)
        sessionView.layer.addSublayer(trackLayer)
        sessionView.layer.addSublayer(shapeLayer)
        sessionView.addSubview(checkOutLabel)
        sessionView.addSubview(timeLabel)

        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.3
        animation.duration = 0.6
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        pulsatingLayer.add(animation, forKey: "pulsing")
        
//        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
//        basicAnimation.toValue = 0.0
//        basicAnimation.duration = 0.6
//        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
//        basicAnimation.isRemovedOnCompletion = true
//        shapeLayer.add(basicAnimation, forKey: "loading")
    }

    @objc func handleCheckIn() {
        checkInAction?()
    }
    
    @objc func handleCheckOut() {
        checkOutAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
