//
//  File.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/29/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
    let sessionApi = SessionApi()
    let pactApi = PactApi()
    let formatter = DateComponentsFormatter()
    let totalSessionTime = 5.0
    let timerInterval = 0.1
    
    var homeView: HomeView!
    var menuLauncher: MenuLauncher!
    var timer: Timer?
    var timeUntilCheckout: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        userDefaults.printData()
        
        homeView = HomeView(frame: self.view.frame)
        view.addSubview(homeView)
        homeView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        
        homeView.checkInAction = checkInPressed
        homeView.checkOutAction = checkOutPressed

        updateProgress()
        showInitialSessionButton()
        
        NavigationManager.showDefault(controller: self, animated: true, action: #selector(returnHome))
        NavigationManager.addMenuButton(controller: self, action: #selector(handleMenu))
        
        menuLauncher = MenuLauncher(homeController: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func updateProgress() {
        guard let dayCount = userDefaults.pact?.dayCount else { return }
        sessionApi.getWeeklySessionCount() { [weak self] (count) in
            if count == dayCount {
                self?.homeView.progressLabel.text = "You've completed your pact for this week."
            } else {
                self?.homeView.progressLabel.text = "You've completed \(count) out of \(dayCount) work outs this week."
            }
        }
    }
    
    func timesStr(num: Int) -> String {
        if num == 1 {
            return "once"
        } else if num == 2 {
            return "twice"
        }
        return "\(num) times"
    }
    
    func showInitialSessionButton() {
        if let (userCheckInTime, _) = userDefaults.checkIn {
            timeUntilCheckout = totalSessionTime - (Date().timeIntervalSince1970 - userCheckInTime)
            if timeUntilCheckout <= 0 {
                homeView.showCheckOutButton()
            } else {
                homeView.showProgressLayers()
                handleTimer()
                timer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: true)
            }
        } else {
            homeView.showCheckInButton()
        }
    }
    
    func checkInPressed() {
        guard let gymDistance = locationManager.currentGymDistance else { return }
        let gymInt = Int(gymDistance)
        let range = 75
        if gymInt > range {
            let alert = UIAlertController(title: "Out of Range", message: "You must be within \(range) meters from your gym to check in. You are currently \(gymInt) meters away.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        } else {
            let checkInTime = Date().timeIntervalSince1970
            guard let gymLocation = userDefaults.gymLocation else { return }
            userDefaults.checkIn = (checkInTime, gymLocation)
            timeUntilCheckout = totalSessionTime
            homeView.showProgressLayers()
            handleTimer()
            timer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: true)
        }
    }
    
    func checkOutPressed() {
        let alert = UIAlertController(title: "Congrats!", message: "You have completed a workout.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true)
        
        sessionApi.logSession()
        homeView.showCheckInButton()
        updateProgress()
    }
    
    @objc func handleTimer() {
        timeUntilCheckout = timeUntilCheckout - timerInterval
        let formattedString = formatter.string(from: TimeInterval(timeUntilCheckout))!
        homeView.timeLabel.text = formattedString
        homeView.shapeLayer.strokeEnd = 0.83 - (0.83 * CGFloat(timeUntilCheckout/totalSessionTime))
        if (timeUntilCheckout <= 0.0) {
            timer?.invalidate()
            homeView.showCheckOutButton()
            homeView.shapeLayer.strokeEnd = 0.0
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func updatePactPressed() {
        self.navigationController?.pushViewController(PactController(), animated: true)
    }
    
    @objc func returnHome() {
        menuLauncher.selectHome()
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func handleMenu() {
        menuLauncher.launchMenu()
    }
}
