//
//  TestLocationView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/24/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class TestLocationView: UIView {
    var searchAction: (() -> Void)?

    let searchButton: UIButton = {
        let button = UIButton(title: "Search")
        button.addTarget(self, action: #selector(handleSearch), for: .touchUpInside)
        return button
    }()

    let distanceLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.text = "Distance: Infinite"
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let smartDistanceLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.text = "Smart Distance: Infinite"
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let accuracyLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.text = "Accuracy: Infinite"
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setup() {
        let stackView = createStackView(views: [searchButton, distanceLabel, smartDistanceLabel, accuracyLabel])
        addSubview(stackView)
        
        backgroundColor = .white
        
        stackView.setAnchor(width: self.frame.width - 60, height: 310)
        stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    @objc func handleSearch() {
        searchAction?()
    }
    
    func setDistanceLabelText(text: Double) {
        distanceLabel.text = "Distance: " + String(format: "%.3f", text)
    }
    
    func setSmartDistanceLabelText(text: Double) {
        smartDistanceLabel.text = "Smart Distance: " + String(format: "%.3f", text)
    }
    
    func setAccuracyLabelText(text: Double) {
        accuracyLabel.text = "Accuracy: " + String(format: "%.3f", text)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
