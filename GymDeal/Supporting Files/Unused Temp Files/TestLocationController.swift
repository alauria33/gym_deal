//
//  TestLocationController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit
import MapKit
import GooglePlaces

class TestLocationController: UIViewController, CLLocationManagerDelegate {
    let authorizationApi = AuthorizationApi()
    let HOME_LAT:Double = 40.734358;
    let HOME_LON:Double = -73.975237;
    let SYNERGY_LAT:Double = 40.736965;
    let SYNERGY_LON:Double = -73.980535;
    
    let defaults = UserDefaults.standard
    
    var user: User? {
        didSet {
            print("value set")
            guard let userName = user?.name else { return }
            navigationItem.title = userName
        }
    }
    
    var locationManager = CLLocationManager()
    var testLocationView: TestLocationView!

    var smartData = SmartLocationData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Your Name"
        navigationController?.navigationBar.barTintColor = UIColor(r: 124, g: 132, b: 132)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "LogOut", style: .plain, target: self, action: #selector(logOut))

        setupView()
//
//        FirebaseApi.shared.fetch() { [weak self] (user) in
//            if user != nil {
//                self?.user = user
//            }
//        }
//
//        initializeLocationManager()
    }

    func setupView() {
        self.testLocationView = TestLocationView(frame: self.view.frame)
        self.view.addSubview(testLocationView)
        testLocationView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        self.testLocationView.searchAction = searchPressed
    }
    
    func searchPressed() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.navigationController?.navigationBar.barTintColor = .yellow
        autocompleteController.navigationController?.navigationBar.tintColor = .red
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func logOut() {
        authorizationApi.logout { (error) in
            if error != nil {
                print("ERROR")
            } else {
                let loginController = UINavigationController(rootViewController: LoginController())
                present(loginController, animated: true, completion: nil)
            }
        }
    }
    
    func initializeLocationManager() {
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            //locationManager.distanceFilter = 20 //?
            /*
             locationManager.pausesLocationUpdatesAutomatically = true //?
             locationManager.activityType = .fitness //?
            */
            locationManager.startUpdatingLocation()
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined:
                    // Request when-in-use authorization initially
                    locationManager.requestAlwaysAuthorization()
                    break
                case .restricted, .denied:
                    print("Always authorized denied")
                    break
                case .authorizedAlways:
                    print("Always authorized")
                    if #available(iOS 9.0, *) {
                        //locationManager.allowsBackgroundLocationUpdates = true
                    }
                    break
                default:
                    break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        let locValue:CLLocationCoordinate2D = lastLocation.coordinate
        let lat = locValue.latitude
        let lon = locValue.longitude
        let accuracy = lastLocation.horizontalAccuracy
//        print("----------------------------------------------------")
//        print("location = \(lat) \(lon)")
//        print("desired accuracy: \(locationManager.desiredAccuracy)")
//        print("accuracy = \(accuracy) m")
//        print("distance filter = \(locationManager.distanceFilter) m")
        let dist = TestLocationController.distance(lat1: lat, lon1: lon, lat2: SYNERGY_LAT, lon2: SYNERGY_LON)
//        print("distance to synergy = \(dist) m")
        testLocationView.setDistanceLabelText(text: dist)
        testLocationView.setAccuracyLabelText(text: lastLocation.horizontalAccuracy)
        
        smartData.add(lat: lat, lon: lon, accuracy: accuracy)
        let smartDistance = smartData.getDistance(lat: SYNERGY_LAT, lon: SYNERGY_LON)
        testLocationView.setSmartDistanceLabelText(text: smartDistance)
    }
    
    class func distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> Double {
        let earthRadius:Double = 6371000.0; //meters
        
        let dLat = degreesToRadians(degrees: lat2-lat1);
        let dLon = degreesToRadians(degrees: lon2-lon1);
        
        let rLat1 = degreesToRadians(degrees: lat1);
        let rLat2 = degreesToRadians(degrees: lat2);
        
        let a = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(rLat1) * cos(rLat2);
        let c = 2 * atan2(sqrt(a), sqrt(1.0 - a));
        
        return earthRadius * c;
    }
    
    class func degreesToRadians(degrees: Double) -> Double {
        return degrees * Double.pi/180.0;
    }

    struct SmartLocationData {
        fileprivate let MAX_SIZE = 5
        fileprivate var array: [(Double, Double, Double)] = []
        
        var location: (Double, Double) {
            get {
                return (0.0, 0.0)
            }
        }
        
        mutating func add(lat: Double, lon: Double, accuracy: Double) {
            array.append((lat, lon, accuracy))
            if (array.count > MAX_SIZE) {
                array.removeFirst()
            }
        }
        
        func getDistance(lat: Double, lon: Double) -> Double {
            print("--------------------------")
            print(array)
            var accuracyTotal = 0.0
            for data in array {
                let accuracy = pow(1/data.2, 2)
                accuracyTotal += accuracy
            }
            var smartLat = 0.0
            var smartLon = 0.0
            var weightTotal = 0.0
            for data in array {
                let accuracy = pow(1/data.2, 2)
                let weight = accuracy/accuracyTotal
                weightTotal += weight
                smartLat += data.0 * weight
                smartLon += data.1 * weight
            }
            print(weightTotal)
//            assert(abs(weightTotal - 1.0) < 0.001
            if (abs(weightTotal - 1.0) > 0.001) {
                return -1.0
            }
            return distance(lat1: smartLat, lon1: smartLon, lat2: lat, lon2: lon)
        }
    }
}

extension TestLocationController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place types: \(place.types)")
//        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
