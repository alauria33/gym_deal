//
//  LocationSearchController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/25/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit
import GooglePlaces

class LocationSearchController: UIViewController, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating, UITableViewDataSource, UITableViewDelegate {
    private var searchController: UISearchController!
    private var myTableView: UITableView!
    private var tableItems: [String] = []
    private var placesClient: GMSPlacesClient!
    
    private let color1 = UIColor(r: 194, g: 198, b: 206)
    private let color2 = UIColor(r: 74, g: 219, b: 81)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient()
        view.backgroundColor = color1
        navigationController?.setNavigationBarHidden(true, animated: false)
        setUpSearchController()
        setUpTableView()

        print("LOADING SEARCH CONTROLLER")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("1: \(self.searchController.searchBar.canBecomeFirstResponder)")
        DispatchQueue.main.async {
            print("Running dispatch")
            print("2: \(self.searchController.canBecomeFirstResponder)")
            self.searchController.isActive = true
            print("3: \(self.searchController.searchBar.canBecomeFirstResponder)")
        }
    }

    func setUpSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchController.searchBar.backgroundImage = UIImage()
        searchController.searchBar.backgroundColor = color1
        searchController.searchBar.barTintColor = color1
        searchController.searchBar.tintColor = .black
        searchController.searchBar.layer.borderColor = color1.cgColor
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.resignFirstResponder()
//        navigationController?.navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
    }

    func setUpTableView() {
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height

        print("Bar height: \(barHeight)")
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.tableHeaderView = searchController.searchBar
        self.view.addSubview(myTableView)
    }
    
    // called whenever text is changed.
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        placesClient.autocompleteQuery("Sydney Oper", bounds: nil, filter: filter, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
        tableItems.append(searchText)
        if tableItems.count > 6 {
            tableItems = []
        }
        DispatchQueue.main.async {
            self.myTableView.reloadData()
        }
    }
    
    // called when cancel button is clicked
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.text = ""
        self.searchController.searchBar.endEditing(true)
    }
    
    // called when search button is clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print(searchController.searchBar.text!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Num: \(indexPath.row)")
        print("Value: \(tableItems[indexPath.row])")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(tableItems[indexPath.row])"
        return cell
    }
    
    func presentSearchController(_ searchController: UISearchController) {
        self.searchController.searchBar.becomeFirstResponder()
    }
}
