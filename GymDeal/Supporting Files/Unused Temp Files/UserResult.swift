//
//  UserResult.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/15/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Foundation

class UserResult {
    var id: Int
    var completedSessions: Int
    var pledgedSessions: Int
    var betAmount: Double
    
    init(id: Int, completedSessions: Int, pledgedSessions: Int, betAmount: Double) {
        self.id = id
        self.completedSessions = completedSessions
        self.pledgedSessions = pledgedSessions
        self.betAmount = betAmount
    }
}
