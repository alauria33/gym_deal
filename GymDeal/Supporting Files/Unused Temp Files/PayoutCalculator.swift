//
//  PayoutCalculator.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/15/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Foundation

class PayoutCalculator {
    let DEFAULT_BET_CONSTANT = 0.85
    let DEFAULT_SESSION_CONSTANT = 0.15
    
    var betConstant: Double
    var sessionConstant: Double
    
    init(betConstant: Double, sessionConstant: Double) {
        self.betConstant = betConstant
        self.sessionConstant = sessionConstant
    }
    
    init() {
        self.betConstant = DEFAULT_BET_CONSTANT
        self.sessionConstant = DEFAULT_SESSION_CONSTANT
    }
    
    func calculate(userResults: [UserResult]) -> [Int:Double] {
        var userPayouts = [Int:Double]()
        
        var winners = [UserResult]()
        var payoutTotal = 0.0
        var winnerBetTotal = 0.0
        var winnerSessionTotal = 0
        for userResult in userResults {
            let completionRate = Double(userResult.completedSessions)/Double(userResult.pledgedSessions)
            if (completionRate == 1) {
                winnerSessionTotal += userResult.completedSessions
                winnerBetTotal += userResult.betAmount
                winners.append(userResult)
            } else {
                let loserPayout = completionRate * userResult.betAmount
                let loserContribution = userResult.betAmount - loserPayout
                payoutTotal += loserContribution
                userPayouts[userResult.id] = loserPayout
            }
        }
        
        for winner in winners {
            let betFactor = winner.betAmount/winnerBetTotal
            let sessionFactor = Double(winner.completedSessions)/Double(winnerSessionTotal)
            let winnerPayout = payoutTotal * (betConstant*betFactor + sessionConstant*sessionFactor)
            userPayouts[winner.id] = winner.betAmount + winnerPayout
        }
        
        return userPayouts
    }
}
