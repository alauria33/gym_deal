//
//  MenuRow.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/16/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class MenuRow {
    var labelName: String
    var imageName: String
    var controller: UIViewController? {
        get {
            if labelName == "Home" {
                return HomeController()
            } else if labelName == "Pact" {
                return PactController()
            } else if labelName == "History" {
                return HistoryController()
            }
            
            let blankController = UIViewController()
            blankController.view.backgroundColor = .backgroundColor
            return blankController
        }
    }
    
    init(labelName: String, imageName: String) {
        self.labelName = labelName
        self.imageName = imageName
    }
}
