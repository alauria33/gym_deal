//
//  NavigationManager.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class NavigationManager {
    class func hide(controller: UIViewController) {
        controller.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    class func push(old: UIViewController, new: UIViewController, animated: Bool) {
        new.navigationItem.hidesBackButton = true
        old.navigationController?.pushViewController(new, animated: animated)
    }

    class func showDefault(controller: UIViewController, animated: Bool, action: Selector?) {
        guard let navController = controller.navigationController else { return }
        navController.setNavigationBarHidden(false, animated: animated)
        
        let navBar = navController.navigationBar
        navBar.barTintColor = UIColor.navigationGreen
        navBar.layer.zPosition = 10
        
        let w = navBar.frame.width * 0.18
        let h = navBar.frame.height * 0.58
        
        let iconButton = UIButton(type: .system)
        if action != nil {
            iconButton.addTarget(controller, action: action!, for: .touchUpInside)
        }
        navBar.addSubview(iconButton)
        iconButton.setAnchor(width: w, height: h)
        iconButton.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        iconButton.centerYAnchor.constraint(equalTo: navBar.centerYAnchor).isActive = true
        
        let iconImageView = UIImageView(image: UIImage(named: "dumbbell"))
        iconImageView.contentMode = .scaleAspectFit
        iconButton.addSubview(iconImageView)
        iconImageView.setAnchor(width: w * 0.6, height: h * 0.7)
        iconImageView.centerXAnchor.constraint(equalTo: iconButton.centerXAnchor).isActive = true
        iconImageView.centerYAnchor.constraint(equalTo: iconButton.centerYAnchor).isActive = true
    }
    
    class func addMenuButton(controller: UIViewController, action: Selector) {
        guard let navBar = controller.navigationController?.navigationBar else { return }
        
        let w = navBar.frame.width * 0.14
        let h = navBar.frame.height * 0.65
        
        let menuButton = UIButton(type: .system)
        menuButton.addTarget(controller, action: action, for: .touchUpInside)
        navBar.addSubview(menuButton)
        menuButton.setAnchor(width: w, height: h)
        menuButton.leftAnchor.constraint(equalTo: navBar.leftAnchor, constant: navBar.frame.width * 0.015).isActive = true
        menuButton.centerYAnchor.constraint(equalTo: navBar.centerYAnchor).isActive = true
        
        let menuImageView = UIImageView(image: UIImage(named: "menu"))
        menuImageView.contentMode = .scaleAspectFit
        menuButton.addSubview(menuImageView)
        menuImageView.setAnchor(width: w * 0.5, height: h * 0.65)
        menuImageView.centerXAnchor.constraint(equalTo: menuButton.centerXAnchor).isActive = true
        menuImageView.centerYAnchor.constraint(equalTo: menuButton.centerYAnchor).isActive = true
    }
    
    class func addSubmitButton(controller: UIViewController, action: Selector) {
        guard let navBar = controller.navigationController?.navigationBar else { return }
        
        let w = navBar.frame.width * 0.2
        let h = navBar.frame.height * 0.65
        
        let submitButton = UIButton(type: .system)
        submitButton.setTitle("Submit", for: .normal)
        submitButton.titleLabel?.textAlignment = .right
        submitButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        submitButton.addTarget(controller, action: action, for: .touchUpInside)
        submitButton.tintColor = .white
        navBar.addSubview(submitButton)
        submitButton.setAnchor(width: w, height: h)
        submitButton.rightAnchor.constraint(equalTo: navBar.rightAnchor, constant: navBar.frame.width * -0.014).isActive = true
        submitButton.centerYAnchor.constraint(equalTo: navBar.centerYAnchor).isActive = true
    }
}
