//
//  MenuCell.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class MenuCell: BaseCell {
    let nameLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: 16)
        l.textColor = .darkColor
        return l
    }()
    
    let iconImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.tintColor = .darkColor
        return iv
    }()
    
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = .white
        
        addSubview(iconImageView)
        
        addSubview(nameLabel)
        nameLabel.setAnchor(width: self.frame.width * 0.8, height: self.frame.height * 0.8)
        nameLabel.leftAnchor.constraint(equalTo: iconImageView.leftAnchor, constant: self.frame.width * 0.15).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor, constant: self.frame.height * 0.005).isActive = true
    }
    
    func set(menuRow: MenuRow) {
        nameLabel.text = menuRow.labelName
        
        let image = UIImage(named: menuRow.imageName)
        let templateImage = image?.withRenderingMode(.alwaysTemplate)
        iconImageView.image = templateImage
        if menuRow.labelName == "Pact" {
            iconImageView.setAnchor(width: self.frame.width * 0.09, height: self.frame.width * 0.09)
            iconImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.frame.width * 0.0675).isActive = true
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        } else if menuRow.labelName == "Reminders" {
            iconImageView.setAnchor(width: self.frame.width * 0.09, height: self.frame.width * 0.09)
            iconImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.frame.width * 0.067).isActive = true
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        } else {
            iconImageView.setAnchor(width: self.frame.width * 0.08, height: self.frame.width * 0.08)
            iconImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: self.frame.width * 0.07).isActive = true
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                backgroundColor = .lightColor
                nameLabel.textColor = .darkColor
                iconImageView.tintColor = .darkColor
            } else {
                backgroundColor = .white
                nameLabel.textColor = .darkColor
                iconImageView.tintColor = .darkColor
            }
            
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                backgroundColor = .lightColor
                nameLabel.textColor = .darkColor
                iconImageView.tintColor = .darkColor
            } else {
                backgroundColor = .white
                nameLabel.textColor = .darkColor
                iconImageView.tintColor = .darkColor
            }
                
        }
    }
}

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
