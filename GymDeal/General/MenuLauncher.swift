//
//  MenuLauncher.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class MenuLauncher: UICollectionViewFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    var homeController: UIViewController!
    var menuView: MenuView!
    let cellId = "cellId"
    var currentControllerName: String!
    
    let menuRows = [
        MenuRow(labelName: "Home", imageName: "home"),
        MenuRow(labelName: "Pact", imageName: "handshake3"),
        MenuRow(labelName: "Reminders", imageName: "reminders"),
        MenuRow(labelName: "History", imageName: "history"),
        MenuRow(labelName: "Official Rules", imageName: "rules2"),
        MenuRow(labelName: "Account", imageName: "profile")
    ]
    
    convenience init(homeController: UIViewController) {
        self.init()
        self.homeController = homeController
        self.currentControllerName = "Home"
        
        menuView = MenuView()
        menuView.frame = homeController.view.frame
        menuView.dismissAction = handleDismiss
        homeController.navigationController?.view.addSubview(menuView)
        menuView.setup()
        
        menuView.collectionView.dataSource = self
        menuView.collectionView.delegate = self
        //        itemSize = CGSize(width: menuView.collectionView.frame.width, height:  menuView.collectionView.frame.height/3.0)
        menuView.collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellId)
        
        selectHome()
    }
    
    func selectHome() {
        currentControllerName = "Home"
        menuView.collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.top)
    }
    
    func launchMenu() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.menuView.blackView.alpha = 1
            self.menuView.isUserInteractionEnabled = true
            self.menuView.slideOutView.frame.origin = CGPoint(x: 0, y: 0)
        })
    }
    
    func handleDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveLinear, animations: {
            self.menuView.blackView.alpha = 0
            self.menuView.isUserInteractionEnabled = false
            self.menuView.slideOutView.frame.origin = CGPoint(x: -1.0 * self.menuView.slideOutView.frame.width, y: 0)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuRows.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MenuCell
        cell.set(menuRow: menuRows[indexPath.item])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        handleDismiss()
        
        let menuRow = menuRows[indexPath.item]
        let newName = menuRow.labelName
        if newName == currentControllerName {
            return
        }
        homeController.navigationController?.popViewController(animated: false)
        currentControllerName = newName
        if newName != "Home" {
            guard let newController = menuRow.controller else { return }
            NavigationManager.push(old: homeController, new: newController, animated: false)
        }
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
