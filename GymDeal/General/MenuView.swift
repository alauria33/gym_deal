//
//  MenuView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/14/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class MenuView: UIView {
    var dismissAction: (() -> Void)?
    
    let gradientHeight = CGFloat(0.35)
    
    var blackView: UIView!
    var slideOutView: UIView!
    var collectionView: UICollectionView!
    
    func setup() {
        layer.zPosition = 20
        isUserInteractionEnabled = false
        setUpBlackView()
        setUpSlideOutView()
    }
    
    func setUpBlackView() {
        blackView = UIView()
        blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
        blackView.frame = frame
        blackView.alpha = 0.0
        addSubview(blackView)
    }
    
    func setUpSlideOutView() {
        slideOutView = UIView()
        slideOutView.backgroundColor = .white
        let w = frame.width * 0.75
        slideOutView.frame = CGRect(x: -1.0 * w, y: 0, width: w, height: frame.height)
        addSubview(slideOutView)
        
        addGradient()
        addTitle()
        addCollectionView()
    }
    
    func addGradient() {
//        let gradient = CAGradientLayer()
//        gradient.frame = CGRect(x: 0, y: 0, width: slideOutView.frame.width, height: slideOutView.frame.height)
//        gradient.colors = [UIColor.mediumGreen.cgColor, UIColor.white.cgColor]//UIColor.backgroundColor.cgColor]
//        gradient.startPoint = CGPoint(x: 0.5, y: 0.07)
//        gradient.endPoint = CGPoint(x: 0.5, y: gradientHeight * 1.15)
//        slideOutView.layer.addSublayer(gradient)
//
//        let lineView = UIView()
//        lineView.backgroundColor = .lightColor
//        slideOutView.addSubview(lineView)
//        let lineHeight = CGFloat(0.001)
//        lineView.setAnchor(width: slideOutView.frame.width, height: slideOutView.frame.height * lineHeight)
//        lineView.topAnchor.constraint(equalTo: self.topAnchor, constant: slideOutView.frame.height * (gradientHeight - lineHeight)).isActive = true
//        lineView.leftAnchor.constraint(equalTo: slideOutView.leftAnchor).isActive = true
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: slideOutView.frame.width, height: slideOutView.frame.height)
        gradient.colors = [UIColor.mediumGreen.cgColor, UIColor.white.cgColor]//UIColor.backgroundColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.1)
        gradient.endPoint = CGPoint(x: 0.5, y: gradientHeight * 1.0)
        slideOutView.layer.addSublayer(gradient)
    }
    
    func addTitle() {
//        let title = UIImageView()
//        title.image = UIImage(named:"title")
//        slideOutView.addSubview(title)
//        title.setAnchor(width: slideOutView.frame.width * 0.58, height: slideOutView.frame.height * 0.128)
//        title.centerXAnchor.constraint(equalTo: slideOutView.centerXAnchor).isActive = true
//        title.topAnchor.constraint(equalTo: slideOutView.topAnchor, constant: self.frame.height * 0.08).isActive = true
//        let templateImage = title.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
//        title.image = templateImage
////        title.tintColor = .backgroundColor
//        title.tintColor = .white
//
        
        let title = UIImageView()
        title.image = UIImage(named:"title")
        slideOutView.addSubview(title)
        title.setAnchor(width: slideOutView.frame.width * 0.58, height: slideOutView.frame.height * 0.128)
//        title.setAnchor(width: slideOutView.frame.width * 0.435, height: slideOutView.frame.height * 0.096)
        title.centerXAnchor.constraint(equalTo: slideOutView.centerXAnchor).isActive = true
        title.topAnchor.constraint(equalTo: slideOutView.topAnchor, constant: self.frame.height * 0.12).isActive = true
        let templateImage = title.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        title.image = templateImage
        //        title.tintColor = .backgroundColor
        title.tintColor = .backgroundColor
    }
    
    func addCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: slideOutView.frame.width, height:  (0.06 * slideOutView.frame.height))
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        collectionView.backgroundColor = UIColor.backgroundColor
        collectionView.backgroundColor = UIColor.white
        let h = slideOutView.frame.height * 0.6
        collectionView.frame = CGRect(x: 0, y: gradientHeight * slideOutView.frame.height, width: slideOutView.frame.width, height: 0.6 * slideOutView.frame.height)
        slideOutView.addSubview(collectionView)
    }
    
    @objc func handleDismiss() {
        dismissAction?()
    }
}
