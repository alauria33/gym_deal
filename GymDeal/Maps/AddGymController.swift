//
//  AddGymView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/27/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import GooglePlaces
import Firebase
import UIKit

class AddGymController: UIViewController {
    var showAlert = false
    var addGymView: AddGymView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.backgroundColor
        setupView()
        
        NavigationManager.showDefault(controller: self, animated: true, action: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if showAlert {
            showAlert = false
            let alert = UIAlertController(title: "Invalid Selection", message: "This location is not a registered gym. Please make another selection.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //
    //        if self.isMovingFromParent {
    //            Auth.auth().currentUser?.delete { error in
    //                if let error = error {
    //                    print(error)
    //                } else {
    //                    // Account deleted.
    //                }
    //            }
    //        }
    //    }
    
    func setupView() {
        self.addGymView = AddGymView(frame: self.view.frame)
        self.addGymView.addAction = addPressed
        self.view.addSubview(addGymView)
        addGymView.setAnchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    func addPressed() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.boundAutocomplete(distance: 8000)
//        let searchBarTextAttributes: [NSAttributedString.Key : AnyObject] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        autocompleteController.primaryTextHighlightColor = UIColor.buttonGreen
        present(autocompleteController, animated: true, completion: nil)
    }
}

extension AddGymController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        if place.types.contains("gym") {
            dismiss(animated: true, completion: nil)
            let backItem = UIBarButtonItem()
            backItem.tintColor = .white
            self.navigationController?.pushViewController(MapController.create(location: place.coordinate), animated: true)
        } else {
            showAlert = true
            dismiss(animated: true, completion: nil)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
