//
//  MapController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 11/13/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit
import MapKit

class MapController: UIViewController, MKMapViewDelegate {
    let authorizationApi = AuthorizationApi()
    var mapView: MKMapView!
    var location: CLLocationCoordinate2D?
    
    class func create(location: CLLocationCoordinate2D) -> MapController {
        let controller = MapController()
        controller.location = location
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        mapView = MKMapView()
        mapView.frame = view.frame
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.center = view.center
        view.addSubview(mapView)
        
        if let coordinate = location {
            let annotation = MKPointAnnotation()
            annotation.title = ""
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
            let timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: false)
        }
        
        NavigationManager.addSubmitButton(controller: self, action: #selector(handleSubmit))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func handleTimer() {
        if let coordinate = location {
            let viewRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: 700, longitudinalMeters: 700)
            mapView.setRegion(viewRegion, animated: true)
        }
    }
    
    @objc func handleSubmit() {
        if let coordinate = location {
            authorizationApi.updateGymLocation(location: coordinate)
        }
        let navController = UINavigationController(rootViewController: PactController())
        self.present(navController, animated: true)
    }
}
