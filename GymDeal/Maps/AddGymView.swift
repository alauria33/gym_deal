//
//  SignUpView.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/19/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import UIKit

class AddGymView: UIView {
    var addAction: (() -> Void)?
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Add your Gym"
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 34)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.textAlignment = .justified
        label.textColor = .black
        label.numberOfLines = 5
        label.text = "This information will be used to allow you to check in and register your work outs!"
        label.text = "In order to register your work outs, you must first add your gym. Your gym's location will be used to determine if you are actually at a gym when you attempt to check in."
        return label
    }()
    
    let addButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("  Add  ", for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 21)
        button.backgroundColor = UIColor.buttonGreen
        button.tintColor = .white
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(handleAdd), for: .touchUpInside)
        return button
    }()
    
    func setup() {
        backgroundColor = UIColor.backgroundColor
        
        self.addSubview(titleLabel)
        titleLabel.setAnchor(width: self.frame.width, height: self.frame.height * 0.08)
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height * -0.3).isActive = true
        
        self.addSubview(descriptionLabel)
        descriptionLabel.setAnchor(width: self.frame.width * 0.85, height: self.frame.height * 0.2)
        descriptionLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        descriptionLabel.centerYAnchor.constraint(equalTo: self.titleLabel.centerYAnchor, constant: self.frame.height * 0.15).isActive = true
        
        self.addSubview(addButton)
        addButton.setAnchor(width: self.frame.width * 0.4, height: self.frame.height * 0.06)
        addButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        addButton.centerYAnchor.constraint(equalTo: self.descriptionLabel.centerYAnchor, constant: self.frame.height * 0.18).isActive = true
    }
    
    @objc func handleAdd() {
        addAction?()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
