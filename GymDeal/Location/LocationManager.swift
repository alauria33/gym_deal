//
//  TestLocationController.swift
//  GymDeal
//
//  Created by Andrew Lauria on 10/22/18.
//  Copyright © 2018 Andrew Lauria. All rights reserved.
//

import Firebase
import UIKit
import MapKit
import GooglePlaces

class LocationManager: NSObject, CLLocationManagerDelegate {
    private let smart = true;

    private let HOME_LAT = 40.734358;
    private let HOME_LON = -73.975237;
    private let SYNERGY_LAT = 40.736965;
    private let SYNERGY_LON = -73.980535;

    private var manager = CLLocationManager()
    private var smartData = SmartLocationData()
    
    var currentLocation: CLLocationCoordinate2D?
    var currentSmartLocation: CLLocationCoordinate2D?
    
    var currentGymDistance: Double? {
        get {
            guard let location = smart ? currentSmartLocation : currentLocation else { return nil }
            guard let gymLocation = userDefaults.gymLocation else { return nil }
            return distance(location1: location, location2: gymLocation)
        }
    }
    
    var currentSynergyDistance: Double? {
        get {
            guard let location = smart ? currentSmartLocation : currentLocation else { return nil }
            return distance(lat1: location.latitude, lon1: location.longitude, lat2: SYNERGY_LAT, lon2: SYNERGY_LON)
        }
    }
    
    var currentHomeDistance: Double? {
        get {
            guard let location = smart ? currentSmartLocation : currentLocation else { return nil }
            return distance(lat1: location.latitude, lon1: location.longitude, lat2: HOME_LAT, lon2: HOME_LON)
        }
    }
    
    func initialize() {
        manager.allowsBackgroundLocationUpdates = true
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            /*
             manager.pausesLocationUpdatesAutomatically = true //?
             manager.activityType = .fitness //?
             */
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                // Request when-in-use authorization initially
                manager.requestAlwaysAuthorization()
                break
            case .restricted, .denied:
                break
            case .authorizedAlways:
                if #available(iOS 9.0, *) {
                    //locationManager.allowsBackgroundLocationUpdates = true
                }
                break
            default:
                break
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else { return }
        currentLocation = lastLocation.coordinate
        if let location = currentLocation {
            currentSmartLocation = smartData.update(newLocation: location, accuracy: lastLocation.horizontalAccuracy)
        }
    }
    
    func distance(location1: CLLocationCoordinate2D, location2: CLLocationCoordinate2D) -> Double {
        return distance(lat1: location1.latitude, lon1: location1.longitude, lat2: location2.latitude, lon2: location2.longitude)
    }
    
    func distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> Double {
        let earthRadius:Double = 6371000.0; //meters
        
        let dLat = degreesToRadians(degrees: lat2-lat1);
        let dLon = degreesToRadians(degrees: lon2-lon1);
        
        let rLat1 = degreesToRadians(degrees: lat1);
        let rLat2 = degreesToRadians(degrees: lat2);
        
        let a = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(rLat1) * cos(rLat2);
        let c = 2 * atan2(sqrt(a), sqrt(1.0 - a));
        
        return earthRadius * c;
    }
    
    func degreesToRadians(degrees: Double) -> Double {
        return degrees * Double.pi/180.0;
    }
    
    struct SmartLocationData {
        fileprivate let MAX_SIZE = 5
        fileprivate var array: [(CLLocationCoordinate2D, Double)] = []
        
        var location: CLLocationCoordinate2D?
        
        mutating func update(newLocation: CLLocationCoordinate2D, accuracy: Double) -> CLLocationCoordinate2D {
            array.append((newLocation, accuracy))
            if (array.count > MAX_SIZE) {
                array.removeFirst()
            }
            
            var accuracyTotal = 0.0
            for data in array {
                let accuracy = pow(1/data.1, 2)
                accuracyTotal += accuracy
            }
            var smartLat = 0.0
            var smartLon = 0.0
            for data in array {
                let accuracy = pow(1/data.1, 2)
                let weight = accuracy/accuracyTotal
                smartLat += data.0.latitude * weight
                smartLon += data.0.longitude * weight
            }
            
            location = CLLocationCoordinate2D(latitude: smartLat, longitude: smartLon)
            return location!
        }
    }
}
